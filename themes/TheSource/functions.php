<?php 

require_once(TEMPLATEPATH . '/epanel/custom_functions.php'); 

require_once(TEMPLATEPATH . '/includes/functions/comments.php'); 

require_once(TEMPLATEPATH . '/includes/functions/sidebars.php'); 

load_theme_textdomain('TheSource',get_template_directory().'/lang');

require_once(TEMPLATEPATH . '/epanel/options_thesource.php');

require_once(TEMPLATEPATH . '/epanel/core_functions.php'); 

require_once(TEMPLATEPATH . '/epanel/post_thumbnails_thesource.php');

function register_main_menus() {
   register_nav_menus(
      array(
         'primary-menu' => __( 'Primary Menu' ),
         'secondary-menu' => __( 'Secondary Menu' ),
		 'footer-menu' => __( 'Footer Menu' ),
      )
   );
};
if (function_exists('register_nav_menus')) add_action( 'init', 'register_main_menus' );

$wp_ver = substr($GLOBALS['wp_version'],0,3);
if ($wp_ver >= 2.8) include(TEMPLATEPATH . '/includes/widgets.php'); ?>