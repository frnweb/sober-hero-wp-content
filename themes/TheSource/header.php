<?php global $default_colorscheme, $shortname, $category_menu, $exclude_pages, $exclude_cats, $hide, $strdepth, $strdepth2, $page_menu; ?>
<?php $colorSchemePath = '';
	  $colorScheme = get_option($shortname . '_color_scheme');
      if ($colorScheme <> $default_colorscheme) $colorSchemePath = strtolower($colorScheme) . '/'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php elegant_titles(); ?></title>
<?php elegant_description(); ?>
<?php elegant_keywords(); ?>
<?php elegant_canonical(); ?>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style.css" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/ie6style.css" />
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/DD_belatedPNG_0.0.8a-min.js"></script>
	<script type="text/javascript">DD_belatedPNG.fix('img#logo, #cat-nav-left, #cat-nav-right, #search-form, #cat-nav-content, div.top-overlay, .slide .description, div.overlay, a#prevlink, a#nextlink, .slide a.readmore, .slide a.readmore span, .recent-cat .entry .title, #recent-posts .entry p.date, .footer-widget ul li, #tabbed-area ul#tab_controls li span');</script>
<![endif]-->
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/ie7style.css" />
<![endif]-->
<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/ie8style.css" />
<![endif]-->

<script type="text/javascript">
	document.documentElement.className = 'js';
</script>

<!-- LiveHelpNow! flyout button code -->
     
    <script type="text/javascript">
            var lhnVersion = 5.3;
            var lhnAccountN = 14160;
            var lhnButtonN = -1;
            var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
            var lhnInviteEnabled = 1;
            var lhnInviteChime = 0;
            var lhnWindowN = 14960;
            var lhnDepartmentN = 0;
            var lhnCustomInvitation = '';
            var lhnCustom1 = '';
            var lhnCustom2 = '';
            var lhnCustom3 = '';
            var lhnTrackingEnabled = 't';
            var lhnTheme = "green";
            var lhnHPPanel = true;
            var lhnHPKnowledgeBase = false;
            var lhnHPMoreOptions = true;
            var lhnHPChatButton = true;
            var lhnHPTicketButton = true;
            var lhnHPCallbackButton = false;
            var lhnLO_helpPanel_knowledgeBase_find_answers = "Find answers";
            var lhnLO_helpPanel_knowledgeBase_please_search = "Please search our knowledge base for answers or click [More options]";
            var lhnLO_helpPanel_typeahead_noResults_message = "No results found for";
            var lhnLO_helpPanel_typeahead_result_views = "views";
            var loadLHNFile = function (url, type) { if (type == "js") { var file = document.createElement('script'); file.setAttribute("type", "text/javascript"); file.setAttribute("src", url); } else if (type = "css") { var file = document.createElement('link'); file.setAttribute("rel", "stylesheet"); file.setAttribute("type", "text/css"); file.setAttribute("href", url); } if (typeof file != "undefined") { document.getElementsByTagName('head')[0].appendChild(file) } }
            var loadLHNFiles = function () {
                            if (lhnHPChatButton == true && typeof lhnInstalled == "undefined") {
                                   if (!document.getElementById('lhnChatButton')) {
                                                   if (document.body) {
                                                                   var lhnBTNdiv = document.createElement('div');
                                                                    lhnBTNdiv.id = 'lhnChatButton';
                                                                    if (document.body.lastChild) { document.body.insertBefore(lhnBTNdiv, document.body.lastChild); } else { document.body.appendChild(lhnBTNdiv); }
                                                    }
                                                    else {
                                                                    document.write('<div id="lhnChatButton"></div>');
                                                    }
                                    }
                                    loadLHNFile(lhnJsHost + 'www.livehelpnow.net/lhn/scripts/livehelpnow.aspx?lhnid=' + lhnAccountN + '&iv=' + lhnInviteEnabled + '&d=' + lhnDepartmentN + '&ver=' + lhnVersion + '&rnd=' + Math.random(), "js");
                                    window.setTimeout("if (typeof bLHNOnline != 'undefined' && bLHNOnline == 0 && lhnHPPanel == true){document.getElementById('lhn_live_chat_btn').style.display='none';}", 2000);
                            }
                loadLHNFile(lhnJsHost + "www.livehelpnow.net/lhn/js/build/helppanel.ashx", "js");
                loadLHNFile(lhnJsHost + "www.foundationsrecoverynetwork.com/support/style.css", "css"); //You may use your CSS here, these classes must be used: http://foundationsrecoverynetwork.com/support/style.css
                    }
            if (window.addEventListener) {
                    window.addEventListener('load', function () {
                            loadLHNFiles();
                    }, false);
            } else if (window.attachEvent) {
                    window.attachEvent('onload', function () {
                            loadLHNFiles();
                    });
            }
     
                    jQuery(document).ready(function(){
                            setTimeout(function() {
                                    jQuery('.lhn_help_container').show();
                                    jQuery('.lhn_btn_container').append('<span><div class="flyout-phone">1.800.256.8799</div><div class="flyout-bottom">Confidential & Private</div></span>');
                                    jQuery('.lhn_help_container').hide();
                            }, 3000);
                           
                    });
     
    </script>
     
    <!-- End LiveHelpNow! flyout button code -->

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>

<!-- BEGIN FUSION TAG CODE - DO NOT EDIT! --><div id="swifttagcontainer36pwbwtug8"><div id="proactivechatcontainer36pwbwtug8"></div><div style="display: inline;" id="swifttagdatacontainer36pwbwtug8"></div></div> <script type="text/javascript">var swiftscriptelem36pwbwtug8=document.createElement("script");swiftscriptelem36pwbwtug8.type="text/javascript";var swiftrandom = Math.floor(Math.random()*1001); var swiftuniqueid = "36pwbwtug8"; var swifttagurl36pwbwtug8="https://support.foundationsrecoverynetwork.com/visitor/index.php?/Default/LiveChat/HTML/SiteBadge/cHJvbXB0dHlwZT1jaGF0JnVuaXF1ZWlkPTM2cHdid3R1ZzgmdmVyc2lvbj00LjQwLjgzMyZwcm9kdWN0PUZ1c2lvbiZzaXRlYmFkZ2Vjb2xvcj13aGl0ZSZiYWRnZWxhbmd1YWdlPWVuJmJhZGdldGV4dD1saXZlaGVscCZvbmxpbmVjb2xvcj0jMTk4YzE5Jm9ubGluZWNvbG9yaG92ZXI9IzVmYWY1ZiZvbmxpbmVjb2xvcmJvcmRlcj0jMTI2MjEyJm9mZmxpbmVjb2xvcj0jYTJhNGFjJm9mZmxpbmVjb2xvcmhvdmVyPSNiZWMwYzUmb2ZmbGluZWNvbG9yYm9yZGVyPSM3MTczNzgmYXdheWNvbG9yPSM3MzdjNGEmYXdheWNvbG9yaG92ZXI9IzllYTQ4MSZhd2F5Y29sb3Jib3JkZXI9IzUxNTczNCZiYWNrc2hvcnRseWNvbG9yPSM3ODhhMjMmYmFja3Nob3J0bHljb2xvcmhvdmVyPSNhMWFlNjYmYmFja3Nob3J0bHljb2xvcmJvcmRlcj0jNTQ2MTE5JmN1c3RvbW9ubGluZT0mY3VzdG9tb2ZmbGluZT0mY3VzdG9tYXdheT0mY3VzdG9tYmFja3Nob3J0bHk9CmZkYTQwMjBmNDg0OGE1OGUxODc5ZGIzNjcyMzkyNGVmNTFmNzJlZWI=";setTimeout("swiftscriptelem36pwbwtug8.src=swifttagurl36pwbwtug8;document.getElementById('swifttagcontainer36pwbwtug8').appendChild(swiftscriptelem36pwbwtug8);",1);</script><!-- END FUSION TAG CODE - DO NOT EDIT! -->

<script type='text/javascript'>var _sf_startpt=(new Date()).getTime()</script>

</head>
<body<?php if (is_home()) echo(' id="home"'); ?>>
	<div id="header-top" class="clearfix">
		<div class="container clearfix">
			<!-- Start Logo -->			
			<?php if ( $colorScheme == 'Light' || $colorScheme == 'Red' || $colorScheme == 'Blue') $colorFolder = $colorSchemePath; ?>
		
			<!-- 
			<a href="<?php bloginfo('url'); ?>">
				<?php $logo = (get_option('thesource_logo') <> '') ? get_option('thesource_logo') : get_bloginfo('template_directory').'/images/'.$colorFolder.'logo.png'; ?>
				<img src="<?php echo $logo; ?>" alt="Logo" id="logo"/>
			</a>
			End Old Logo Code -->

			<a href="<?php bloginfo('url'); ?>">
				<img src="http://www.soberhero.com/wp-content/uploads/SoberHeroHeader.png" alt="Logo" id="logo"/>
			</a>
			
			<p id="slogan"><?php bloginfo('description'); ?></p>
			<!-- End Logo -->
			
			
			
			<div id="cat-nav" class="clearfix">	
				<div id="cat-nav-left"> </div>
				<div id="cat-nav-content"> 
					
					<?php $menuClass = 'nav clearfix';
					$primaryNav = '';
					
					if (function_exists('wp_nav_menu')) $primaryNav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'echo' => false ) );
					if ($primaryNav == '') show_page_menu($menuClass);
					else echo($primaryNav); ?>

					<!-- Start Searchbox -->
					<div id="search-form">
						<form method="get" id="searchform1" action="<?php bloginfo('url'); ?>">
							<input type="text" value="<?php _e('search...','TheSource'); ?>" name="s" id="searchinput" />
		
							<input type="image" src="<?php bloginfo('template_directory'); ?>/images/<?php echo($colorSchemePath); ?>search_btn.png" id="searchsubmit" />
						</form>
					</div>
				<!-- End Searchbox -->	
				</div> <!-- end #cat-nav-content -->
				<div id="cat-nav-right"> </div>
			</div>	<!-- end #cat-nav -->	
		</div> 	<!-- end .container -->
	</div> 	<!-- end #header-top -->
	
	
	
	<?php if ( (is_home() || is_front_page()) && get_option('thesource_featured') == 'on' ) include(TEMPLATEPATH . '/includes/featured.php'); ?>
	
	<div id="content">
		<?php if (!is_home()) { ?>
			<div id="content-top-shadow"></div>
		<?php }; ?>
		<div class="container">