<!-- Start Featured -->	
<div id="featured">

	<?php global $ids;
	$ids = array();
	$arr = array();
	$i=1;
			
	$featured_cat = get_option('thesource_feat_cat');
	$featured_num = get_option('thesource_featured_num');
	
	if (get_option('thesource_use_pages') == 'false') query_posts("showposts=$featured_num&cat=".get_catId($featured_cat));
	else { 
		global $pages_number;
		
		if (get_option('thesource_feat_pages') <> '') $featured_num = count(get_option('thesource_feat_pages'));
		else $featured_num = $pages_number;
		
		query_posts(array
						('post_type' => 'page',
						'orderby' => 'menu_order',
						'order' => 'ASC',
						'post__in' => get_option('thesource_feat_pages'),
						'showposts' => $featured_num
					));
	};
	
	while (have_posts()) : the_post(); ?>
		<?php $bgColor = get_post_meta($post->ID, 'Color',true);
		if ($bgColor == '') $bgColor = 'ffffff'; ?>

		<?php $width = 1400;
			  $height = 300;
			  $classtext = '';
			  $titletext = get_the_title();
				
			  $thumbnail = get_thumbnail($width,$height,$classtext,$titletext,$titletext,true);
			  $thumb = $thumbnail["thumb"]; ?>
		
		<div class="slide" style="background: #000000 url('<?php print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext, $width, $height, '', true, true); ?>') top center no-repeat;">
			<div class="container clearfix">
				 <!-- end .description -->			
			</div> 	<!-- end .container -->	
			
			<div class="top-overlay"></div>
			<div class="overlay"></div>
			
		</div> <!-- end .slide -->
		
	<?php $i++;
	$ids[]= $post->ID;
	endwhile; wp_reset_query();	?>
		
</div> <!-- end #featured -->
<!-- End Featured -->


<div id="featured-control">
	<div class="container">
		<a id="prevlink" href="#"><?php _e('Previous','TheSource'); ?></a>
		<a id="nextlink" href="#"><?php _e('Next','TheSource'); ?></a>
	</div>
</div> <!-- end #featured-control -->