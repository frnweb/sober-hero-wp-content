<div class="top<?php if($last) echo(" last"); ?>">
   <h4 class="title"><?php echo(stripslashes(get_option($cat_option))); ?></h4>
</div>

<?php $thumb = '';      

   $width = 239;
   $height = 133;
   $classtext = 'thumb';
   $titletext = get_the_title();
   
   $thumbnail = get_thumbnail($width,$height,$classtext,$titletext,$titletext);
   $thumb = $thumbnail["thumb"]; ?>

<?php if ($thumb <> '') print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext , $width, $height, $classtext); ?>

<div class="entry <?php echo($headingColor); ?>"<?php if ($thumb == '') echo(' style="padding-top: 70px;"'); ?>>
   <div class="title"<?php if ($thumb == '') echo(' style="top: 13px;"'); ?>>
      <h3><?php the_title(); ?></h3>
   </div>
   
   <p><?php truncate_post(182); ?></p>
   
</div>